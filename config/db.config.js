const env = require('./env');

const Sequelize = require('sequelize');
const sequelize = new Sequelize(env.database, env.username, env.password, {
    host: env.host,
    dialect: env.dialect,
    operatorsAliases: false,

    pool: {
        max: env.pool.max,
        min: env.pool.min,
        acquire: env.pool.acquire,
        idle: env.pool.idle
    }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.user = require('../models/User2.js')(sequelize, Sequelize);
db.instrument = require('../models/Instrument.js')(sequelize, Sequelize);
db.company = require('../models/Company.js')(sequelize, Sequelize);
db.project = require('../models/Project.js')(sequelize, Sequelize);

db.user.hasMany(db.instrument, {foreignKey: 'fk_userid', sourceKey: id});
db.instrument.belongsTo(db.user, {foreignKey: 'fk_userid', targetKey: id});

sequelize
    .authenticate()
    .then(() => {
        console.log('Connection has been established successfully.');
    })
    .catch(err => {
        console.error('Unable to connect to the database:', err);
    });

module.exports = db;