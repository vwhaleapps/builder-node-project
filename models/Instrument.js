module.exports = (sequelize, Sequelize) => {
    return sequelize.define('instrument', {
        id: {
            type: Sequelize.INTEGER,
            allowNull: false,
            autoIncrement: true,
            primaryKey: true,
        },
        title: {
            type: Sequelize.STRING,
            allowNull: false
        },
        model: {
            type: Sequelize.STRING,
            allowNull: false,
            unique: true,
        },
        ean13: {
            type: Sequelize.STRING,
            unique: true,
        },
        upc: {
            type: Sequelize.STRING,
            unique: true,
        },
        description: {
            type: Sequelize.STRING,
            unique: true,
        },
        createdAt: Sequelize.DATE,
        updatedAt: Sequelize.DATE
    });
};