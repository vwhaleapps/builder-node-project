// generated with sequelize model:generate --name User --attributes email:STRING,firstName:STRING,lastName:STRING,pass
// word:STRING,role:ENUM,createAt:DATE,updateAt:DATE
'use strict';
module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    email: DataTypes.STRING,
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    password: DataTypes.STRING,
    role: DataTypes.ENUM,
    createAt: DataTypes.DATE,
    updateAt: DataTypes.DATE
  }, {});
  User.associate = function(models) {
    // 1 to many with board
    User.hasMany(models.Compa)
    // associations can be defined here
  };
  return User;
};