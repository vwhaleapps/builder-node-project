var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', (req, res, next) => {
    res.status(200).json({
        message: 'Handling GET /users Request'
    });
});

router.post('/', (req, res, next) => {
    const user = {
        firstname: req.body.firstname,
        lastname: req.body.lastname,
        password: req.body.password,
        email: req.body.email,
        dob: req.body.dob,
        role: req.body.role
    };
    res.status(201).json({
        message: 'Handling POST /users Request'
    });
});

router.get('/:userId', (req, res, next) => {
    res.status(200).json({
        message: 'Handling POST /users/id Request'
    });
});

router.patch('/:userId', (req, res, next) => {
    res.status(200).json({
        message: 'Updated User'
    });
});

router.delete('/:userId', (req, res, next) => {
    res.status(200).json({
        message: 'Deleted User'
    });
});

module.exports = router;