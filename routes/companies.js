var express = require('express');
var router = express.Router();

/* GET companies listing. */
router.get('/', (req, res, next) => {
    res.status(200).json({
        message: 'Handling GET /companies Request'
    });
});

router.post('/', (req, res, next) => {
    const companie = {
        userId: req.body.userId,
        title: req.body.title,
        description: req.body.description,
        location: req.body.location
    };
    res.status(201).json({
        message: 'Handling POST /companies Request'
    });
});

router.get('/:companyId', (req, res, next) => {
    res.status(200).json({
        message: 'Handling GET /companies by id Request',
        companyId: req.params.companyId
    });
});

router.delete('/:companyId', (req, res, next) => {
    res.status(200).json({
        message: 'Handling DELETE/companies by id Request',
        companyId: req.params.companyId
    });
});

module.exports = router;